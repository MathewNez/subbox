from django.urls import path
from . import views

app_name = 'crudbox'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('register', views.register_request, name='register'),
    path('login', views.login_request, name='login'),
    path('logout', views.logout_request, name='logout'),
    path('add_box', views.add_box_request, name='add_box'),
    path('add_subscription/<pk>/', views.add_subscription_request, name='add_subscription'),
    path('subscriptions', views.view_subscriptions_request, name='subscriptions')
]
