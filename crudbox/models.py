from django.db import models
from django.contrib.auth.models import User


class Box(models.Model):
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=250)
    price = models.FloatField()
    seller_id = models.ForeignKey(User, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='pics')


class Subscription(models.Model):
    subscriber_id = models.ForeignKey(User, on_delete=models.CASCADE)
    box_id = models.ForeignKey(Box, on_delete=models.CASCADE)
    date_start = models.DateField()
    date_end = models.DateField()
    days_period = models.IntegerField()
