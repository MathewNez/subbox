from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from crudbox.models import Box, Subscription


class NewUserForm(UserCreationForm):

    class Meta:
        model = User
        fields = ('username', 'password1', 'password2')


class BoxForm(forms.ModelForm):
    seller_id = forms.ModelChoiceField(queryset=User.objects.all(), widget=forms.HiddenInput())

    class Meta:
        model = Box
        fields = ('name', 'description', 'price', 'image', 'seller_id')


class SubscriptionForm(forms.ModelForm):
    subscriber_id = forms.ModelChoiceField(queryset=User.objects.all(), widget=forms.HiddenInput())
    box_id = forms.ModelChoiceField(queryset=Box.objects.all(), widget=forms.HiddenInput())
    date_start = forms.DateField(widget=forms.SelectDateWidget())
    date_end = forms.DateField(widget=forms.SelectDateWidget())

    class Meta:
        model = Subscription
        fields = ('date_start', 'date_end', 'days_period', 'subscriber_id', 'box_id')
