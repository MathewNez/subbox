from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .forms import NewUserForm, BoxForm, SubscriptionForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm

from .models import Subscription, Box


def register_request(request):
    if request.method == 'POST':
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, 'Registration success')
            return redirect('crudbox:homepage')
        messages.error(request, 'Registration unsuccessful, invalid credentials')
    form = NewUserForm()
    return render(request=request, template_name='register.html', context={'register_form': form})


def login_request(request):
    if request.method == 'POST':
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f'Logged in a {username}')
            else:
                messages.error(request, f'Invalid username or password')
        else:
            messages.error(request, 'Invalid username or password')
    form = AuthenticationForm()
    return render(request, 'login.html', context={'login_form': form})


def logout_request(request):
    logout(request)
    messages.info(request, 'Successfully logged out a user')
    return redirect('crudbox:homepage')


def homepage(request):
    boxes = Box.objects.all()
    return render(request, 'homepage.html', context={'boxes': boxes})


@login_required
def add_box_request(request):
    if request.method == 'POST':
        form = BoxForm(request.POST, request.FILES)
        print(form.errors)
        if form.is_valid():
            form.save()
            return redirect('crudbox:homepage')
    form = BoxForm(initial={'seller_id': request.user})
    return render(request, 'box_form.html', context={'box_form': form})


@login_required
def add_subscription_request(request, pk):
    if request.method == 'POST':
        form = SubscriptionForm(request.POST)
        print(form.errors)
        if form.is_valid():
            form.save()
            return redirect('crudbox:subscriptions')
    box = Box.objects.get(pk=pk)
    form = SubscriptionForm(initial={'subscriber_id': request.user, 'box_id': box})
    return render(request, 'add_subscription.html', context={'add_subscription_form': form, 'box': box})


@login_required
def view_subscriptions_request(request):
    subscriptions = Subscription.objects.filter(subscriber_id=request.user.pk)
    return render(request, 'subscriptions.html', context={'subscriptions': subscriptions})

