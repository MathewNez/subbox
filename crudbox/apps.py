from django.apps import AppConfig


class CrudboxConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'crudbox'
